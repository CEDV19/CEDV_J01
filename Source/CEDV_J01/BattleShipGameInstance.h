// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "ConfigCacheIni.h"
#include "BattleShipGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class CEDV_J01_API UBattleShipGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
	public:
		UBattleShipGameInstance();

		UFUNCTION(BlueprintCallable)
		TArray<FString> LoadBestRecords();

		UFUNCTION(BlueprintCallable)
		void SaveBestRecords();

		UFUNCTION(BlueprintCallable)
		void SetNewRecord(FString name, int score);

		// Variables
		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		TArray<FString> records;

		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordName1;

		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordName3;

		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordName2;

		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordName4;

		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordScore1;

		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordScore2;

		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordScore3;

		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordScore4;

		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString currentName;

		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString currentScore;

	protected:

		// variables
		FString configRecordsSection;
		FString recordName1Key;
		FString recordName3Key;
		FString recordName2Key;
		FString recordName4Key;
		FString recordScore1Key;
		FString recordScore2Key;
		FString recordScore3Key;
		FString recordScore4Key;
};
