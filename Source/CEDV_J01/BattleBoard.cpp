// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleBoard.h"
#include "Components/StaticMeshComponent.h"
#include "math.h"

// Sets default values
ABattleBoard::ABattleBoard() : AActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	if (!isAI)
	{
		isMyTurn = true;
	}
	else
	{
		isMyTurn = false;
	}
}

// Called when the game starts or when spawned
void ABattleBoard::BeginPlay()
{
	Super::BeginPlay();

	// Create Ships
	TWeakObjectPtr<ABattleShip> battleShip;
	battleShip = GetWorld()->SpawnActor<ABattleShip>();
	battleShip->SetShipParameters(2);
	ships.Add(battleShip.Get());

	battleShip = GetWorld()->SpawnActor<ABattleShip>();
	ships.Add(battleShip.Get());

	battleShip = GetWorld()->SpawnActor<ABattleShip>();
	ships.Add(battleShip.Get());

	battleShip = GetWorld()->SpawnActor<ABattleShip>();
	battleShip->SetShipParameters(4);
	ships.Add(battleShip.Get());

	battleShip = GetWorld()->SpawnActor<ABattleShip>();
	battleShip->SetShipParameters(5);
	ships.Add(battleShip.Get());

	// Copy Cells list to know how many cells left
	boardCellsNotSelected = boardCells;

	// Randomize its position
	SetShipsPositions();
}

// Called every frame
void ABattleBoard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*int randomNumber = FMath::RandRange(0, 10);
	FString stringg(FString::FromInt(randomNumber));
	UE_LOG(LogTemp, Warning, TEXT("%s"), *stringg);*/
}

void ABattleBoard::SetTurn(bool myTurn)
{
	isMyTurn = myTurn;
}

bool ABattleBoard::IsAllShipsDestroyed()
{
	bool allShipsDestroyed = true;
	for (ABattleShip* ship : ships)
	{
		if (!ship->IsDestroyed())
		{
			allShipsDestroyed = false;
			break;
		}
	}
	return allShipsDestroyed;
}

bool ABattleBoard::IsAllBoardDamaged()
{
	return boardCellsNotSelected.Num() == 0;
}

void ABattleBoard::SetShipsPositions()
{
	bool recalculateRandomPosition;
	for (ABattleShip* ship : ships) {
		recalculateRandomPosition = true;
		while (recalculateRandomPosition)
		{
			// Get random positions and direction
			int32 randomCell = FMath::RandRange(0, 99);
			int32 randomDirection = FMath::RandRange(0, 3); // Random direction: 0 Left, 1 Up, 2 Right and 3 Down

			// Get the column and row to set the ship
			uint8 column = randomCell / 10;
			uint8 row = randomCell % 10;

			// Check if is posiible to insert and insert
			TWeakObjectPtr<ABattleBoardCell> cell = boardCells[randomCell];
			if (randomDirection == 0) // Left
			{
				if (ship->size <= column + 1)
				{
					uint8 newCell = randomCell;
					bool cellsWithoutShipOrWrong = true;
					TArray<ABattleBoardCell*> auxiliaryCellList;
					for (uint8 indexSize = 0; indexSize < ship->size; ++indexSize)
					{
						if (!cell->HasShip())
						{
							auxiliaryCellList.Add(cell.Get());
							newCell -= 10;
							if (newCell < 0 || newCell > 99) {
								cellsWithoutShipOrWrong = false;
								break;
							}
							cell = boardCells[newCell];
						}
						else
						{
							cellsWithoutShipOrWrong = false;
							break;
						}
					}
					if (cellsWithoutShipOrWrong)
					{
						for (ABattleBoardCell* correctCell : auxiliaryCellList)
						{
							correctCell->SetShip(ship, !isAI);
						}
						recalculateRandomPosition = false;
					}
				}
			}
			else if (randomDirection == 1) // Up
			{
				if (ship->size <= row + 1)
				{
					uint8 newCell = randomCell;
					bool cellsWithoutShipOrWrong = true;
					TArray<ABattleBoardCell*> auxiliaryCellList;
					for (uint8 indexSize = 0; indexSize < ship->size; ++indexSize)
					{
						if (!cell->HasShip())
						{
							auxiliaryCellList.Add(cell.Get());
							newCell -= 1;
							if (newCell < 0 || newCell > 99) {
								cellsWithoutShipOrWrong = false;
								break;
							}
							cell = boardCells[newCell];
						}
						else
						{
							cellsWithoutShipOrWrong = false;
							break;
						}
					}
					if (cellsWithoutShipOrWrong)
					{
						for (ABattleBoardCell* correctCell : auxiliaryCellList)
						{
							correctCell->SetShip(ship, !isAI);
						}
						recalculateRandomPosition = false;
					}
				}
			}
			else if (randomDirection == 2) // Right
			{
				if (ship->size + column <= 10)
				{
					uint8 newCell = randomCell;
					bool cellsWithoutShipOrWrong = true;
					TArray<ABattleBoardCell*> auxiliaryCellList;
					for (uint8 indexSize = 0; indexSize < ship->size; ++indexSize)
					{
						if (!cell->HasShip())
						{
							auxiliaryCellList.Add(cell.Get());
							newCell += 10;
							if (newCell < 0 || newCell > 99) {
								cellsWithoutShipOrWrong = false;
								break;
							}
							cell = boardCells[newCell];
						}
						else
						{
							cellsWithoutShipOrWrong = false;
							break;
						}
					}
					if (cellsWithoutShipOrWrong)
					{
						for (ABattleBoardCell* correctCell : auxiliaryCellList)
						{
							correctCell->SetShip(ship, !isAI);
						}
						recalculateRandomPosition = false;
					}
				}
			}
			else // Down
			{
				if (ship->size + row <= 10)
				{
					uint8 newCell = randomCell;
					bool cellsWithoutShipOrWrong = true;
					TArray<ABattleBoardCell*> auxiliaryCellList;
					for (uint8 indexSize = 0; indexSize < ship->size; ++indexSize)
					{
						if (!cell->HasShip())
						{
							auxiliaryCellList.Add(cell.Get());
							newCell += 1;
							if (newCell < 0 || newCell > 99) {
								cellsWithoutShipOrWrong = false;
								break;
							}
							cell = boardCells[newCell];
						}
						else
						{
							cellsWithoutShipOrWrong = false;
							break;
						}
					}
					if (cellsWithoutShipOrWrong)
					{
						for (ABattleBoardCell* correctCell : auxiliaryCellList)
						{
							correctCell->SetShip(ship, !isAI);
						}
						recalculateRandomPosition = false;
					}
				}
			}
		}
	}
}
