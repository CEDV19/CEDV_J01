// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleShipGameState.h"
#include "BattleBoard.h"
#include "RandomAI.h"
#include "Engine.h"

ABattleShipGameState::ABattleShipGameState() : AGameStateBase(), isTurnPlayer(true), numbersOfTurns(0) {}

// Called when the game starts or when spawned
void ABattleShipGameState::BeginPlay()
{
	Super::BeginPlay();

	// Get the boards
	for (TActorIterator<AActor> actorItr(GetWorld()); actorItr; ++actorItr) 
	{
		// Boards
		if (actorItr->GetActorLabel().Equals("BattleBoardPlayer"))
		{
			battleBoardPlayer = Cast<ABattleBoard>(*actorItr);
		}
		else if (actorItr->GetActorLabel().Equals("BattleBoardAI"))
		{
			battleBoardAI = Cast<ABattleBoard>(*actorItr);
		}

		// UI
		else if (actorItr->GetActorLabel().Equals("Turns"))
		{
			turns = actorItr->FindComponentByClass<UTextRenderComponent>();
		}
		else if (actorItr->GetActorLabel().Equals("PlayerTurn"))
		{
			playerTurn = *actorItr;
		}
		else if (actorItr->GetActorLabel().Equals("AITurn"))
		{
			AITurn = *actorItr;
		}
		else if (actorItr->GetActorLabel().Equals("ShipPlayer2Label"))
		{
			shipPlayer2 = actorItr->FindComponentByClass<UTextRenderComponent>();
		}
		else if (actorItr->GetActorLabel().Equals("ShipPlayer3Label"))
		{
			shipPlayer3 = actorItr->FindComponentByClass<UTextRenderComponent>();
		}
		else if (actorItr->GetActorLabel().Equals("ShipPlayer4Label"))
		{
			shipPlayer4 = actorItr->FindComponentByClass<UTextRenderComponent>();
		}
		else if (actorItr->GetActorLabel().Equals("ShipPlayer5Label"))
		{
			shipPlayer5 = actorItr->FindComponentByClass<UTextRenderComponent>();
		}
		else if (actorItr->GetActorLabel().Equals("ShipAI2Label"))
		{
			shipAI2 = actorItr->FindComponentByClass<UTextRenderComponent>();
		}
		else if (actorItr->GetActorLabel().Equals("ShipAI3Label"))
		{
			shipAI3 = actorItr->FindComponentByClass<UTextRenderComponent>();
		}
		else if (actorItr->GetActorLabel().Equals("ShipAI4Label"))
		{
			shipAI4 = actorItr->FindComponentByClass<UTextRenderComponent>();
		}
		else if (actorItr->GetActorLabel().Equals("ShipAI5Label"))
		{
			shipAI5 = actorItr->FindComponentByClass<UTextRenderComponent>();
		}
	}

	// Update UI values
	UpdateUI();

	// Create the AI
	battleShipAI = Cast<IAI>(GetWorld()->SpawnActor<ARandomAI>());
}

void ABattleShipGameState::NextTurn(bool isAShipDamaged)
{
	bool gameIsOver = CheckIfGameIsOver();

	if (!gameIsOver)
	{
		++numbersOfTurns;
		if (!isAShipDamaged)
		{
			isTurnPlayer = !isTurnPlayer;
		}

		UpdateUI();
		if (isTurnPlayer)
		{
			battleBoardPlayer->isMyTurn = true;
			battleBoardAI->isMyTurn = false;
			TurnPlayer();
		}
		else
		{
			battleBoardPlayer->isMyTurn = false;
			battleBoardAI->isMyTurn = true;
			TurnAI();
		}
	}
}

void ABattleShipGameState::UpdateUI()
{
	// Update Player Ships UI
	TArray<ABattleShip*> ships = battleBoardPlayer->ships;
	uint8 ship2 = 0;
	uint8 ship3 = 0;
	uint8 ship4 = 0;
	uint8 ship5 = 0;
	for (ABattleShip* ship : ships)
	{
		if (!ship->IsDestroyed())
		{
			if (ship->size == 2)
			{
				++ship2;
			}
			else if (ship->size == 3)
			{
				++ship3;
			}
			else if (ship->size == 4)
			{
				++ship4;
			}
			else if (ship->size == 5)
			{
				++ship5;
			}
		}
	}
	shipPlayer2->SetText(FText::AsNumber(ship2));
	shipPlayer3->SetText(FText::AsNumber(ship3));
	shipPlayer4->SetText(FText::AsNumber(ship4));
	shipPlayer5->SetText(FText::AsNumber(ship5));

	// Update AI Ships UI
	ships = battleBoardAI->ships;
	ship2 = 0;
	ship3 = 0;
	ship4 = 0;
	ship5 = 0;
	for (ABattleShip* ship : ships)
	{
		if (!ship->IsDestroyed())
		{
			if (ship->size == 2)
			{
				++ship2;
			}
			else if (ship->size == 3)
			{
				++ship3;
			}
			else if (ship->size == 4)
			{
				++ship4;
			}
			else if (ship->size == 5)
			{
				++ship5;
			}
		}
	}
	shipAI2->SetText(FText::AsNumber(ship2));
	shipAI3->SetText(FText::AsNumber(ship3));
	shipAI4->SetText(FText::AsNumber(ship4));
	shipAI5->SetText(FText::AsNumber(ship5));

	// Update Score
	turns->SetText(FText::AsNumber(numbersOfTurns));

	// Update turn tip
	if (isTurnPlayer)
	{
		playerTurn->SetActorHiddenInGame(false);
		AITurn->SetActorHiddenInGame(true);
	}
	else
	{
		playerTurn->SetActorHiddenInGame(true);
		AITurn->SetActorHiddenInGame(false);
	}
}

bool ABattleShipGameState::CheckIfGameIsOver()
{
	if (battleBoardPlayer->IsAllShipsDestroyed())
	{
		UE_LOG(LogTemp, Warning, TEXT("AI Win!"));
		UGameplayStatics::OpenLevel(GetWorld(), "GameOver");
		return true;
	}
	if (battleBoardAI->IsAllShipsDestroyed())
	{
		UE_LOG(LogTemp, Warning, TEXT("Player Win!"));
		GetGameInstance<UBattleShipGameInstance>()->currentScore = FString::FromInt(numbersOfTurns);
		UGameplayStatics::OpenLevel(GetWorld(), "GameWin");
		return true;

	}
	if (battleBoardPlayer->IsAllBoardDamaged() || battleBoardAI->IsAllBoardDamaged())
	{
		UE_LOG(LogTemp, Warning, TEXT("Boards cells full!"));
		UGameplayStatics::OpenLevel(GetWorld(), "GameOver");
		return true;
	}
	return false;
}

void ABattleShipGameState::TurnPlayer()
{
	UE_LOG(LogTemp, Warning, TEXT("Turn Player!"));
}

void ABattleShipGameState::TurnAI()
{
	UE_LOG(LogTemp, Warning, TEXT("Turn AI!"));
	ABattleBoardCell* newBoardCell = battleShipAI->GetNextBoardCell(battleBoardPlayer->boardCells, battleBoardPlayer->boardCellsNotSelected);
	newBoardCell->ClickCell();
}