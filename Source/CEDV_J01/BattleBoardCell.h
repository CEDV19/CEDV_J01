#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "BattleShipGameState.h"
#include "BattleBoardCell.generated.h"

UCLASS()
class CEDV_J01_API ABattleBoardCell : public AActor
{
	GENERATED_BODY()
	
	public:	
		// Sets default values for this actor's properties
		ABattleBoardCell();

		// Called every frame
		virtual void Tick(float DeltaTime) override;

		// Functions of mause events
		UFUNCTION()
		void OnClick(UPrimitiveComponent* touchedComponent , FKey buttonPressed);

		UFUNCTION()
		void OnBeginCursorOver(UPrimitiveComponent* touchedComponent);

		UFUNCTION()
		void OnEndCursorOver(UPrimitiveComponent* touchedComponent);

		UFUNCTION()
		void ClickCell();

		UFUNCTION()
		void SetWater();

		UFUNCTION()
		void SetShip(class ABattleShip* ship, bool showCellColor = true);

		UFUNCTION()
		void SetHit();

		UFUNCTION()
		void SetOverlaps();

		UFUNCTION()
		void SetDefault();

		UFUNCTION()
		bool IsSelected();

		UFUNCTION()
		bool HasShip();

		// Variables
		UPROPERTY(VisibleAnywhere)
		TWeakObjectPtr<UStaticMeshComponent> staticMeshComponent;

		UPROPERTY()
		class ABattleShip* ship;

		UPROPERTY(Category=ClassVariables, EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
		TWeakObjectPtr<class ABattleBoard> battleBoard;

		UPROPERTY(Category=ClassVariables, EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
		TWeakObjectPtr<UMaterialInterface> materialOverlap;

		UPROPERTY(Category=ClassVariables, EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
		TWeakObjectPtr<UMaterialInterface> materialHit;

		UPROPERTY(Category=ClassVariables, EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
		TWeakObjectPtr<UMaterialInterface> materialShip;

		UPROPERTY(Category=ClassVariables, EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
		TWeakObjectPtr<UMaterialInterface> materialWater;

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

		// variables
		bool clicked;

		TWeakObjectPtr<UMaterialInterface> defaultMaterial;

		TWeakObjectPtr<ABattleShipGameState> gameState;
};
