
#include "BattleShipGameMode.h"

ABattleShipGameMode::ABattleShipGameMode() : AGameModeBase() {
	// Set default classes
	PlayerControllerClass = AMousePlayerController::StaticClass();
	GameStateClass = ABattleShipGameState::StaticClass();
}
