
#include "MousePlayerController.h"
#include "GameFramework/FloatingPawnMovement.h"

AMousePlayerController::AMousePlayerController() : APlayerController() {

	// Enable mouse actions
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
}

// Called when the game starts or when spawned
void AMousePlayerController::BeginPlay()
{
	Super::BeginPlay();

	// Disable movements
	InputYawScale = 0;
	InputPitchScale = 0;
	TWeakObjectPtr<UFloatingPawnMovement> movementComponent = GetPawn()->FindComponentByClass<UFloatingPawnMovement>();
	movementComponent->MaxSpeed = 0;
}

void AMousePlayerController::PlayerTick(float DeltaTime) 
{
	Super::PlayerTick(DeltaTime);
}
