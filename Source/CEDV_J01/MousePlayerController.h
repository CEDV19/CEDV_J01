
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MousePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CEDV_J01_API AMousePlayerController : public APlayerController
{
	GENERATED_BODY()

	public:
		AMousePlayerController();

		virtual void PlayerTick(float DeltaTime) override;

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;
};
