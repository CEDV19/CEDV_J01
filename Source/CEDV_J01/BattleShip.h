// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BattleShip.generated.h"

/**
 * 
 */
UCLASS()
class CEDV_J01_API ABattleShip : public AActor
{
	GENERATED_BODY()

	public:
		// Sets default values for this actor's properties
		ABattleShip();

		// Called every frame
		virtual void Tick(float DeltaTime) override;

		UFUNCTION()
		void SetShipParameters(uint8 shipSize = 3);

		UFUNCTION()
		void SetDamage();

		UFUNCTION()
		bool IsDestroyed() const;

		// Variables
		UPROPERTY(Category=ClassVariables, VisibleAnywhere, BlueprintReadWrite, AdvancedDisplay)
		uint8 size;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere, BlueprintReadWrite, AdvancedDisplay)
		uint8 damagedParts;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere, BlueprintReadWrite, AdvancedDisplay)
		bool destroyed;

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;
};
