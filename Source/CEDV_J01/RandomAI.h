// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AI.h"
#include "RandomAI.generated.h"

UCLASS()
class CEDV_J01_API ARandomAI : public AActor, public IAI
{
	GENERATED_BODY()
	
	public:	
		// Sets default values for this actor's properties
		ARandomAI();

		// Called every frame
		virtual void Tick(float DeltaTime) override;

		UFUNCTION()
		virtual ABattleBoardCell* GetNextBoardCell(TArray<ABattleBoardCell*> board, TArray<ABattleBoardCell*> boardCellsLeft) override;

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;
};
