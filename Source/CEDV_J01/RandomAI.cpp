// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomAI.h"

// Sets default values
ARandomAI::ARandomAI() : AActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void ARandomAI::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ARandomAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

ABattleBoardCell* ARandomAI::GetNextBoardCell(TArray<ABattleBoardCell*> board, TArray<ABattleBoardCell*> boardCellsLeft)
{
	// Get random cell and return it
	uint16 numberOfCells = boardCellsLeft.Num();
	uint16 randomCell = FMath::RandRange(0, numberOfCells - 1);
	return boardCellsLeft[randomCell];
}
