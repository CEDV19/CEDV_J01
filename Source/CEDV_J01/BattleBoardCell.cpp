// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleBoardCell.h"
#include "BattleBoard.h"

// Sets default values
ABattleBoardCell::ABattleBoardCell() : AActor(), clicked(false)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create the class components
	staticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	staticMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	// Get relevants Materials
	auto materialRed = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("MaterialInterface'/Game/Materials/BasicShapeMaterialRed'"));
	if (materialRed.Succeeded()) {
		materialHit = materialRed.Object;
	}
	auto materialBlue = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("MaterialInterface'/Game/Materials/BasicShapeMaterialBlue'"));
	if (materialBlue.Succeeded()) {
		materialWater = materialBlue.Object;
	}
	auto materialYellow = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("MaterialInterface'/Game/Materials/BasicShapeMaterialLightYellow'"));
	if (materialYellow.Succeeded()) {
		materialOverlap = materialYellow.Object;
	}
	auto materialBlack = ConstructorHelpers::FObjectFinder<UMaterialInterface>(TEXT("MaterialInterface'/Game/Materials/BasicShapeMaterialBlack'"));
	if (materialBlack.Succeeded()) {
		materialShip = materialBlack.Object;
	}

	// Add mouse events
	staticMeshComponent->OnClicked.AddDynamic(this, &ABattleBoardCell::OnClick);
	staticMeshComponent->OnBeginCursorOver.AddDynamic(this, &ABattleBoardCell::OnBeginCursorOver);
	staticMeshComponent->OnEndCursorOver.AddDynamic(this, &ABattleBoardCell::OnEndCursorOver);
}

// Called when the game starts or when spawned
void ABattleBoardCell::BeginPlay()
{
	Super::BeginPlay();

	// Get default material
	defaultMaterial = staticMeshComponent->GetMaterial(0);

	gameState = GetWorld()->GetGameState<ABattleShipGameState>();
}

// Called every frame
void ABattleBoardCell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABattleBoardCell::OnClick(UPrimitiveComponent* touchedComponent , FKey buttonPressed)
{
	if (battleBoard->isAI && !clicked && !battleBoard->isMyTurn) 
	{
		ClickCell();
	}
}

void ABattleBoardCell::OnBeginCursorOver(UPrimitiveComponent* touchedComponent)
{
	if (battleBoard->isAI && !clicked) 
	{
		SetOverlaps();
	}
}

void ABattleBoardCell::OnEndCursorOver(UPrimitiveComponent* touchedComponent)
{
	if (battleBoard->isAI && !clicked) 
	{
		SetDefault();
	}
}

void ABattleBoardCell::ClickCell()
{
	clicked = true;
	battleBoard->boardCellsNotSelected.Remove(this);
	if (HasShip())
	{
		SetHit();
		ship->SetDamage();
		gameState->NextTurn(true);
	}
	else
	{
		SetWater();
		gameState->NextTurn();
	}
}

void ABattleBoardCell::SetWater()
{
	staticMeshComponent->SetMaterial(0, materialWater.Get());
}

void ABattleBoardCell::SetShip(ABattleShip* ship, bool showCellColor)
{
	this->ship = ship;
	if (showCellColor)
	{
		staticMeshComponent->SetMaterial(0, materialShip.Get());
	}
}

void ABattleBoardCell::SetHit()
{
	staticMeshComponent->SetMaterial(0, materialHit.Get());
}

void ABattleBoardCell::SetOverlaps()
{
	staticMeshComponent->SetMaterial(0, materialOverlap.Get());
}

void ABattleBoardCell::SetDefault()
{
	staticMeshComponent->SetMaterial(0, defaultMaterial.Get());
}

bool ABattleBoardCell::IsSelected()
{
	return clicked;
}

bool ABattleBoardCell::HasShip()
{
	return(ship != nullptr);
}
