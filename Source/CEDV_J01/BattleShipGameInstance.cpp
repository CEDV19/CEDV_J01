// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleShipGameInstance.h"

UBattleShipGameInstance::UBattleShipGameInstance() : 
	UGameInstance(), 
	configRecordsSection("Records"), 
	recordName1Key("RecordName1"), 
	recordName2Key("RecordName2"), 
	recordName3Key("RecordName3"), 
	recordName4Key("RecordName4"),
	recordScore1Key("RecordScore1"),
	recordScore2Key("RecordScore2"),
	recordScore3Key("RecordScore3"),
	recordScore4Key("RecordScore4"),
	recordName1("EMPTY"), 
	recordName2("EMPTY"), 
	recordName3("EMPTY"), 
	recordName4("EMPTY"),
	recordScore1("100000"),
	recordScore2("100000"),
	recordScore3("100000"),
	recordScore4("100000")
{
	FString configFilePath = FPaths::GameSavedDir();
	configFilePath += FString("Config/Windows/Game.ini");
	if (!FPaths::FileExists(configFilePath))
	{
		SaveBestRecords();
	}
	LoadBestRecords();
}

TArray<FString> UBattleShipGameInstance::LoadBestRecords()
{
	GConfig->GetString(*configRecordsSection, *recordName1Key, recordName1, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordName2Key, recordName2, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordName3Key, recordName3, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordName4Key, recordName4, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordScore1Key, recordScore1, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordScore2Key, recordScore2, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordScore3Key, recordScore3, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordScore4Key, recordScore4, GGameIni);
	return records;
}

void UBattleShipGameInstance::SaveBestRecords()
{
	GConfig->SetString(*configRecordsSection, *recordName1Key, *recordName1, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordName2Key, *recordName2, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordName3Key, *recordName3, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordName4Key, *recordName4, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordScore1Key, *recordScore1, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordScore2Key, *recordScore2, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordScore3Key, *recordScore3, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordScore4Key, *recordScore4, GGameIni);
	GConfig->Flush(false, GGameIni);
}

void UBattleShipGameInstance::SetNewRecord(FString name, int score)
{
	if (score < FCString::Atoi(*recordScore1))
	{
		recordName4 = recordName3;
		recordScore4 = recordScore3;
		recordName3 = recordName2;
		recordScore3 = recordScore2;
		recordName2 = recordName1;
		recordScore2 = recordScore1;
		recordName1 = name;
		recordScore1 = FString::FromInt(score);
	}
	else if (score < FCString::Atoi(*recordScore2))
	{
		recordName4 = recordName3;
		recordScore4 = recordScore3;
		recordName3 = recordName2;
		recordScore3 = recordScore2;
		recordName2 = name;
		recordScore2 = FString::FromInt(score);
	}
	else if (score < FCString::Atoi(*recordScore3))
	{
		recordName4 = recordName3;
		recordScore4 = recordScore3;
		recordName3 = name;
		recordScore3 = FString::FromInt(score);
	}
	else if (score < FCString::Atoi(*recordScore4))
	{
		recordName4 = name;
		recordScore4 = FString::FromInt(score);
	}
}
