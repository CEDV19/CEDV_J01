
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MousePlayerController.h"
#include "BattleShipGameState.h"
#include "BattleShipGameMode.generated.h"

/**
 * 
 */
UCLASS()
class CEDV_J01_API ABattleShipGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
	public:
		ABattleShipGameMode();
};
