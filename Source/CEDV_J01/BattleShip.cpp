// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleShip.h"

ABattleShip::ABattleShip() : AActor() 
{
	SetShipParameters();
}

// Called when the game starts or when spawned
void ABattleShip::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABattleShip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABattleShip::SetShipParameters(uint8 shipSize)
{
	size = shipSize;
	damagedParts = 0;
	destroyed = false;
}

void ABattleShip::SetDamage()
{
	if (!destroyed) 
	{
		++damagedParts;
		if (damagedParts == size) 
		{
			destroyed = true;
		}
	}
}

bool ABattleShip::IsDestroyed() const
{
	return destroyed;
}
