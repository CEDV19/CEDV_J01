// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "Components/TextRenderComponent.h"
#include "BattleShipGameInstance.h"
#include "BattleShipGameState.generated.h"

/**
 * 
 */
UCLASS()
class CEDV_J01_API ABattleShipGameState : public AGameStateBase
{
	GENERATED_BODY()
	
	public:
		ABattleShipGameState();

		UFUNCTION()
		void NextTurn(bool isAShipDamaged = false);

		UFUNCTION()
		void UpdateUI();

		// Variables
		UPROPERTY()
		bool isTurnPlayer;

		UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		int numbersOfTurns;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<class ABattleBoard> battleBoardPlayer;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<class ABattleBoard> battleBoardAI;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<UTextRenderComponent> turns;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<AActor> playerTurn;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<AActor> AITurn;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<UTextRenderComponent> shipPlayer2;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<UTextRenderComponent> shipPlayer3;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<UTextRenderComponent> shipPlayer4;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<UTextRenderComponent> shipPlayer5;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<UTextRenderComponent> shipAI2;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<UTextRenderComponent> shipAI3;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<UTextRenderComponent> shipAI4;

		UPROPERTY(Category=ClassVariables, VisibleAnywhere)
		TWeakObjectPtr<UTextRenderComponent> shipAI5;

		class IAI* battleShipAI;

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

		bool CheckIfGameIsOver();

		void TurnPlayer();

		void TurnAI();
};
