// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BattleBoardCell.h"
#include "BattleShip.h"
#include "BattleBoard.generated.h"

UCLASS()
class CEDV_J01_API ABattleBoard : public AActor
{
	GENERATED_BODY()
	
	public:	
		// Sets default values for this actor's properties
		ABattleBoard();

		// Called every frame
		virtual void Tick(float DeltaTime) override;

		UFUNCTION()
		void SetTurn(bool myTurn = true);

		UFUNCTION()
		bool IsAllShipsDestroyed();

		UFUNCTION()
		bool IsAllBoardDamaged();

		UFUNCTION()
		void SetShipsPositions();

		// Execute the functionalities by player or AI
		UPROPERTY(Category=ClassVariables, EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
		bool isAI;

		UPROPERTY(Category=ClassVariables, EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
		bool isMyTurn;

		UPROPERTY(Category=ClassVariables, EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
		TArray<ABattleShip*> ships;

		UPROPERTY(Category=ClassVariables, EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
		TArray<ABattleBoardCell*> boardCells;

		UPROPERTY(Category=ClassVariables, EditAnywhere, BlueprintReadWrite, AdvancedDisplay)
		TArray<ABattleBoardCell*> boardCellsNotSelected;

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;
};
