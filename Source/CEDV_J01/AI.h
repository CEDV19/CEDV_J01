// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "BattleBoardCell.h"
#include "AI.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UAI : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class CEDV_J01_API IAI
{
	GENERATED_BODY()

	public:
		UFUNCTION()
		virtual ABattleBoardCell* GetNextBoardCell(TArray<ABattleBoardCell*> board, TArray<ABattleBoardCell*> boardCellsLeft) = 0;
};
