# Mini-game 01 - Battleship

The first mini-game of the course [expert in video game development](http://cedv.uclm.es/). The game is an simple adaptation of the [Battleship](https://en.wikipedia.org/wiki/Battleship_(game)) game using [Unreal Engine 4](https://www.unrealengine.com).

There is available a [video of the project](https://www.dropbox.com/s/so9nvnl6d2037lg/Minijuego%201%20-%20Battleship.mp4?dl=0).

## Build

Battleship is developed using Unreal Engine 4.21 version. Hence, If you want to open, contribute or use it in your projects, you only need to open the project using this version.

## Pictures

![](https://i.imgur.com/rFVR2nV.png)

## License

Battleship is provided under [GNU General Public License Version 3](https://gitlab.com/CEDV19/CEDV_J01/blob/master/LICENSE).